/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.textView;

import desafio.MainController;
import java.util.Scanner;

/**
 *
 * @author cristian
 */
public class AplicaDescontoCarrinho {

    public static void render(MainController controller) {
        Scanner sc = new Scanner(System.in);  
        System.out.println("Por favor digite o valor do desconto que deseja aplicar ao carrinho.");
        float desconto = sc.nextFloat();
        controller.adicionarDescontoAoCarrinho(desconto);
    }
    
}
