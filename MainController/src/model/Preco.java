/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author cristian
 */
public class Preco {
    
    private double valor = 0;
    private float desconto = 0;

    public double getValor() {
        return valor;
    }
    
    public double getValorComDesconto(){
        return getValorComDuasCasas((valor - valor * (desconto / 100)));
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public float getDesconto() {
        return desconto;
    }

    public void setDesconto(float desconto) {
        if (desconto <= 100 && desconto > 0) {
            this.desconto = desconto;
        } else{
            System.out.println("Log: O valor do desconto deve ser maior que 0 e menor ou igual a 100");
        }
    }

    private double getValorComDuasCasas(double valor) {
        return BigDecimal.valueOf(valor)
                .setScale(3, RoundingMode.HALF_UP)
                .doubleValue();
    }
    
}
