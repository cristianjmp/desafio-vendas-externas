/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.textView;

import desafio.MainController;
import java.util.Map;
import model.Produto;

/**
 *
 * @author cristian
 */
public class ListaProdutosCarrinho {

    public static void render(MainController controller) {
        System.out.println("Estes são os produtos contidos no carrinho atualmente");
        
        Map<Integer, Produto> produtosCarrinho = controller.listarProdutosCarrinho();
        if(produtosCarrinho.size() < 1){
            System.out.println("Nâo existem produtos no carrinho.");
            return;
        }
        ListaProdutos.listarProdutos(produtosCarrinho);
    }
    
}
