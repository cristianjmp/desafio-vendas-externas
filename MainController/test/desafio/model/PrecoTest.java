/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desafio.model;

import model.Preco;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cristian
 */
public class PrecoTest {
    
    @Test
    public void testGetValorComDesconto() {
        System.out.println("getValorComDesconto");
        Preco instance = new Preco();
        instance.setValor(5);
        instance.setDesconto(95);
        double expResult = 0.25;
        double result = instance.getValorComDesconto();
        assertEquals(expResult, result, 0.0);
    }
    
    @Test
    public void testSetDescontoMenorQueZero() {
        System.out.println("get e set Desconto");
        Preco instance = new Preco();
        instance.setDesconto(0);
        float expResult = 0;
        float result = instance.getDesconto();
        assertEquals(expResult, result, 0.0);
    }
    
    @Test
    public void testSetDescontoMaiorQueCem() {
        System.out.println("get e set Desconto");
        Preco instance = new Preco();
        instance.setDesconto(101);
        float expResult = 0;
        float result = instance.getDesconto();
        assertEquals(expResult, result, 0.0);
    }
}
