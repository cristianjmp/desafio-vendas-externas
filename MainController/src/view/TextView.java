/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import desafio.MainController;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import view.textView.AdicionaProdutoCarrinho;
import view.textView.AplicaDescontoCarrinho;
import view.textView.CadastraProduto;
import view.textView.ListaProdutos;
import view.textView.ListaProdutosCarrinho;
import view.textView.RemoveProdutoCarrinho;
import view.textView.TotalizaProdutosCarrinho;

/**
 *
 * @author cristian
 */
public class TextView implements View{

    private final MainController controller;
    
    private static final int SAIR = 0;
    private static final int CADASTRAR_PRODUTO = 1;
    private static final int LISTAR_PRODUTOS = 2;
    private static final int ADICIONAR_PRODUTO_AO_CARRINHO = 3;
    private static final int REMOVER_PRODUTO_DO_CARRINHO = 4;
    private static final int LISTAR_PRODUTOS_DO_CARRINHO = 5;
    private static final int APLICAR_DESCONTO_AO_CARRINHO = 6;
    private static final int TOTALIZA_PRODUTOS_DO_CARRINHO = 7;
    
    private final ArrayList<Integer> opcoesValidasMenu = new ArrayList<>(
        Arrays.asList(SAIR, 
                CADASTRAR_PRODUTO, 
                LISTAR_PRODUTOS, 
                ADICIONAR_PRODUTO_AO_CARRINHO, 
                REMOVER_PRODUTO_DO_CARRINHO, 
                LISTAR_PRODUTOS_DO_CARRINHO,
                APLICAR_DESCONTO_AO_CARRINHO,
                TOTALIZA_PRODUTOS_DO_CARRINHO
            )
    );
    public TextView(MainController controller) {
        this.controller = controller;
    }

    @Override
    public void render() {
        int opcao;
        Scanner sc = new Scanner(System.in);  
        do {
            opcao = renderMenu(sc);
            executaAcaoSelecionada(opcao);
        } while(opcao != SAIR);
    }

    private int renderMenu(Scanner sc) {
        boolean opcaoValida;
        int opcaoMenu = SAIR;
        do {
            System.out.println("Por favor selecione uma das opções a seguir");
            System.out.println(CADASTRAR_PRODUTO + " - Cadastrar produtos");
            System.out.println(LISTAR_PRODUTOS + " - Listar Produtos");
            System.out.println(ADICIONAR_PRODUTO_AO_CARRINHO + " - Adicionar ao carrinho");
            System.out.println(REMOVER_PRODUTO_DO_CARRINHO + " - Remover do Carrinho");
            System.out.println(LISTAR_PRODUTOS_DO_CARRINHO + " - Listar itens do carrinho");
            System.out.println(APLICAR_DESCONTO_AO_CARRINHO + " - Aplicar desconto ao carrinho");
            System.out.println(TOTALIZA_PRODUTOS_DO_CARRINHO + " - Totalizar itens do carrinho");
            System.out.println(SAIR + " - Sair");
            opcaoMenu = sc.nextInt();
            opcaoValida = validaOpcaoMenu(opcaoMenu);
        } while (!opcaoValida);
        return opcaoMenu;
    }

    private boolean validaOpcaoMenu(int opcaoMenu) {
        if(opcoesValidasMenu.contains(opcaoMenu)){
            return true;
        }
        System.out.println("Opção inválida");
        return false;
    }

    private void executaAcaoSelecionada(int opcao) {
        switch (opcao){
            case CADASTRAR_PRODUTO:
                CadastraProduto.render(controller);
                break;
            case LISTAR_PRODUTOS:
                ListaProdutos.render(controller);
                break;
            case ADICIONAR_PRODUTO_AO_CARRINHO:
                AdicionaProdutoCarrinho.render(controller);
                break;
            case REMOVER_PRODUTO_DO_CARRINHO:
                RemoveProdutoCarrinho.render(controller);
                break;
            case LISTAR_PRODUTOS_DO_CARRINHO:
                ListaProdutosCarrinho.render(controller);
                break;
            case APLICAR_DESCONTO_AO_CARRINHO:
                AplicaDescontoCarrinho.render(controller);
                break;
            case TOTALIZA_PRODUTOS_DO_CARRINHO:
                TotalizaProdutosCarrinho.render(controller);
                break;
        }
    }
}
