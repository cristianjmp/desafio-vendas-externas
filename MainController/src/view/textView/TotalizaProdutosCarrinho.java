/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.textView;

import desafio.MainController;

/**
 *
 * @author cristian
 */
public class TotalizaProdutosCarrinho {

    public static void render(MainController controller) {
        double valorTotalCarrinho = controller.totalizarCarrinho();
        System.out.println("O valor total do carrinho é R$ " + valorTotalCarrinho);
    }
    
}
