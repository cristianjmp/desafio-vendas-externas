/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.textView;

import desafio.MainController;
import java.util.List;
import java.util.Map;
import model.Produto;

/**
 *
 * @author cristian
 */
public class ListaProdutos {

    public static void render(MainController controller) {
        Map<Integer, Produto> listaProdutosDisponiveis = controller.listarProdutos();
        if (listaProdutosDisponiveis.size() < 1) {
            System.out.println("Não existem produtos Cadastrados até o monento, por favor efetue o cadastro de alguns produtos");
            return;
        }
        System.out.println("Estes são os produtos cadastrados até o momento");
        listarProdutos(listaProdutosDisponiveis);
    }
    
    
    public static void listarProdutos(Map<Integer, Produto> produtos) {
        for (Map.Entry<Integer, Produto> produto : produtos.entrySet()) {
            System.out.println(produto.getValue().getId() + " - " + produto.getValue().getNome() + " - " + produto.getValue().getPrecoValor()+ " - " + produto.getValue().getPrecoComDesconto());
        }
    }
}
