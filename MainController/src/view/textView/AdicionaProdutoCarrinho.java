/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.textView;

import desafio.MainController;
import java.util.Scanner;

/**
 *
 * @author cristian
 */
public class AdicionaProdutoCarrinho {

    public static void render(MainController controller) {
        Scanner sc = new Scanner(System.in);  
        if (controller.listaProdutosDisponiveis.size() < 1) {
            System.out.println("Não existem produtos cadastrados para serem adicionados ao carrinho, por favor cadastre alguns produtos.");
            return;
        }
        
        boolean adicionouAoCarrinho;
        do {
            System.out.println("Por favor, digite o ID do produto que deseja adicionar ao carrinho");
            ListaProdutos.listarProdutos(controller.listaProdutosDisponiveis);
            int idProduto = sc.nextInt();
            adicionouAoCarrinho = controller.adicionarProdutoAoCarrinho(idProduto);
            
            if (!adicionouAoCarrinho) {
                System.out.println("O produto selecionado não consta na lista de produtos");
            }
        } while (!adicionouAoCarrinho);
    }
    
}
