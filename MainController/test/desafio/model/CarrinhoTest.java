/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desafio.model;

import model.Carrinho;
import model.Produto;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cristian
 */
public class CarrinhoTest {

    @Test
    public void testAddProdutos() {
        System.out.println("addProdutos");
        Carrinho instance = new Carrinho();
        
        Produto produto1 = new Produto(1, "Produto1");
        instance.addProduto(produto1);
        
        Produto produto2 = new Produto(2, "Produto2");
        instance.addProduto(produto2);
        
        int expected = 2;
        assertEquals(expected, instance.getProdutos().size());
    }

    @Test
    public void testGetValorTotalCarrinho() {
        System.out.println("getValorTotalCarrinho");
        Carrinho instance = new Carrinho();
        
        Produto produto1 = new Produto(1, "Produto1");
        produto1.setPreco(5);
        produto1.setDesconto(50);
        instance.addProduto(produto1);
        
        Produto produto2 = new Produto(2, "Produto2");
        produto2.setPreco(10);
        produto2.setDesconto(50);
        instance.addProduto(produto2);
        
        double expResult = 7.5;
        double result = instance.getValorTotalCarrinho();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetValorTotalCarrinhoComDesconto() {
        System.out.println("getValorTotalCarrinhoComDesconto");
        Carrinho instance = new Carrinho();
        instance.setDesconto(50);
        
        Produto produto1 = new Produto(1, "Produto1");
        produto1.setPreco(10);
        produto1.setDesconto(50);
        instance.addProduto(produto1);
        
        Produto produto2 = new Produto(2, "Produto2");
        produto2.setPreco(10);
        produto2.setDesconto(50);
        instance.addProduto(produto2);
        
        double expResult = 5.0;
        double result = instance.getValorTotalCarrinhoComDesconto();
        assertEquals(expResult, result, 0.0);
    }
    
    @Test
    public void testRemoveProdutoDoCarrinho(){
        System.out.println("removeProdutoDoCarrinho");
        Carrinho instance = new Carrinho();
        
        Produto produto1 = new Produto(1, "Produto1");
        instance.addProduto(produto1);
        
        Produto produto2 = new Produto(2, "Produto2");
        instance.addProduto(produto2);
        
        Produto produto3 = new Produto(3, "Produto3");
        instance.addProduto(produto3);
        
        Map produtos = instance.removeProdutoDoCarrinho(3);
        
        assertNull(produtos.get(3));
    }
}
