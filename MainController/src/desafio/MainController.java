package desafio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Carrinho;
import model.Produto;
import view.TextView;
import view.View;

public class MainController {

    public Map<Integer, Produto> listaProdutosDisponiveis = new HashMap();
    private Carrinho carrinhoDeCompras = new Carrinho();
    
    public static void main(String[] args) {
        MainController controller = new MainController();
        View view = new TextView(controller);
        view.render();
    }

    public boolean cadatroProduto(Produto produto) {
        return dataBase.Produto.cadastrarProduto(produto);
    }

    public Map<Integer, Produto> listarProdutos() {
        listaProdutosDisponiveis = dataBase.Produto.getProdutos();
        return listaProdutosDisponiveis;
    }

    public boolean adicionarProdutoAoCarrinho(int idProduto) {
        if (!listaProdutosDisponiveis.containsKey(idProduto)) {
            return false;
        }
        
        carrinhoDeCompras.addProduto(listaProdutosDisponiveis.get(idProduto));
        return true;
    }

    public void adicionarDescontoAoCarrinho(float desconto) {
        carrinhoDeCompras.setDesconto(desconto);
    }

    public Map<Integer, Produto> listarProdutosCarrinho() {
        return carrinhoDeCompras.getProdutos();
    }

    public boolean removerProdutoDoCarrinho(int idProduto) {
        if(carrinhoDeCompras.produtoEstaNoCarrinho(idProduto)){
            carrinhoDeCompras.removeProdutoDoCarrinho(idProduto);
            return true;
        }
        return false;
    }

    public double totalizarCarrinho() {
        return carrinhoDeCompras.getValorTotalCarrinhoComDesconto();
    }
}
