package dataBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class Produto {
 
    public static boolean cadastrarProduto(model.Produto produto) {
        Connection conn = SqLiteConnection.connect();
        if (conn != null) {
            String sqlQuery = "INSERT INTO Produto(nome, preco, desconto) VALUES(?, ?, ?)";
            try {
                PreparedStatement statement = conn.prepareStatement(sqlQuery);
                statement.setString(1, produto.getNome());
                statement.setDouble(2, produto.getPrecoValor());
                statement.setDouble(3, produto.getPrecoComDesconto());
                statement.execute();
                return true;
            } catch (SQLException ex) {
                System.out.println("Falha ao cadastrar o produto: " + ex.getMessage());
            }
        }
        return false;
    }
    
    public static Map<Integer, model.Produto> getProdutos() {
        Connection conn = SqLiteConnection.connect();
        if (conn != null) {
            String sqlQuery = "SELECT * FROM Produto";
            try {
                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sqlQuery);
                return converteRetornoBancoParaListaProdutos(rs);
            } catch (SQLException ex) {
                System.out.println("Falha ao cadastrar o produto: " + ex.getMessage());
            }
        }
        return null;
    }

    private static Map<Integer, model.Produto> converteRetornoBancoParaListaProdutos(ResultSet rs) throws SQLException {
        Map<Integer, model.Produto> listaProduto = new HashMap();
        while (rs.next()) {
            model.Produto produto = new model.Produto(rs.getInt("id"), rs.getString("nome"));
            produto.setPreco(rs.getFloat("preco"));
            produto.setDesconto(rs.getFloat("desconto"));
            listaProduto.put(produto.getId(), produto);
        }
        return listaProduto;
    }
}
