/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.textView;

import desafio.MainController;
import java.util.Map;
import java.util.Scanner;
import model.Produto;

/**
 *
 * @author cristian
 */
public class RemoveProdutoCarrinho {

    public static void render(MainController controller) {
        Scanner sc = new Scanner(System.in);  
        
        Map<Integer, Produto> produtosCarrinho = controller.listarProdutosCarrinho();
        if(produtosCarrinho.size() < 1){
            System.out.println("Nâo existem produtos no carrinho.");
            return;
        }
        
        boolean removeuDoCarrinho;
        do {
            System.out.println("Por favor, digite o ID do produto que deseja adicionar ao carrinho");
            ListaProdutosCarrinho.render(controller);
            int idProduto = sc.nextInt();
            removeuDoCarrinho = controller.removerProdutoDoCarrinho(idProduto);
            
            if (!removeuDoCarrinho) {
                System.out.println("O produto selecionado não consta na lista de produtos");
            }
        } while (!removeuDoCarrinho);
    }
    
}
