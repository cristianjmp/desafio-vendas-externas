/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.textView;

import desafio.MainController;
import java.util.Scanner;
import model.Produto;

/**
 *
 * @author cristian
 */
public class CadastraProduto {

    public static void render(MainController controller) {
        Scanner sc = new Scanner(System.in);  
        
        System.out.println("Bem vindo(a) ao cadastro de produtos, por favor digite o nome do produto:");
        String nome = sc.nextLine();
        System.out.println("Por favor digite o preço do produto:");
        double preco = sc.nextDouble();
        System.out.println("Por favor digite o desconto do produto, caso não queira adiciona um desconto, digite zero:");
        float desconto = sc.nextFloat();
        
        Produto produto = new Produto(nome);
        produto.setPreco(preco);
        produto.setDesconto(desconto);
        
        controller.cadatroProduto(produto);
        System.out.println("Produto cadastrado com sucesso");
    }
    
}
