/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author cristian
 */
public class Carrinho {
 
    private Map<Integer, Produto> produtos = new HashMap();
    private final Preco valorTotalCarrinho = new Preco();

    public Map getProdutos() {
        return produtos;
    }

    public void setProdutos(Map produtos) {
        this.produtos = produtos;
    }

    public void addProduto(Produto produto) {
        inicializaListaProdutos();
        this.produtos.put(produto.getId(), produto);
    }
    
    public void setDesconto(float valorDesconto) {
        valorTotalCarrinho.setDesconto(valorDesconto);
    }
    
    public double getValorTotalCarrinho() {
        calculaValorTotalCarrinho();
        return valorTotalCarrinho.getValor();
    }
    
    public double getValorTotalCarrinhoComDesconto() {
        calculaValorTotalCarrinho();
        return valorTotalCarrinho.getValorComDesconto();
    }

    public Preco calculaValorTotalCarrinho() {
        double valorTotalProdutos = 0;
        for (Map.Entry<Integer, Produto> produto : produtos.entrySet()) {
           valorTotalProdutos += produto.getValue().getPrecoComDesconto();
        }
        valorTotalCarrinho.setValor(valorTotalProdutos);
        return valorTotalCarrinho;
    }
    
    public Map<Integer, Produto> removeProdutoDoCarrinho(int idProduto) {
        produtos.remove(idProduto);
        return produtos;
    }
    
    public boolean produtoEstaNoCarrinho(int idProduto) {
        return produtos.containsKey(idProduto);
    }
    
    private void inicializaListaProdutos() {
        if(produtos == null){
            produtos = new HashMap<>();
        }
    }
    
}
