package model;

public class Produto {
    
    private int id;
    private final String nome;
    private final Preco preco = new Preco();

    public Produto(String nome) {
        this.nome = nome;
    }
    
    public Produto(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }
    
    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public double getPreco() {
        return preco.getValor();
    }

    public void setPreco(double preco) {
        this.preco.setValor(preco);
    }
    
    public void setDesconto(float desconto) {
        preco.setDesconto(desconto);
    }
    
    public double getPrecoValor() {
        return preco.getValor();
    }
    
    public double getPrecoComDesconto() {
        return preco.getValorComDesconto();
    }
}
